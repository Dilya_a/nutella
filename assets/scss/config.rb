require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Set this to the root of your project when deployed:


# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false

#preferred_syntax = :scss
http_path = "/"
css_dir = "../css" # by Compass.app 
sass_dir = "../scss" # by Compass.app 
images_dir = "../images" # by Compass.app 
http_generated_images_dir = "../images/"
javascripts_dir = 'assets/js'
output_style = :expanded # by Compass.app 
relative_assets = true # by Compass.app 
line_comments = true # by Compass.app 
sass_options = {:debug_info=>false} # by Compass.app 
sourcemap = false # by Compass.app 