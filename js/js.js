$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором

// var tabsBox = $(".tabs");

// if(tabsBox.length){
//   include("js/easyResponsiveTabs.js");
// }


// function include(url){ 
//   document.write('<script src="'+ url + '"></script>'); 
// }




$(document).ready(function(){

	$(".resp_menu, .resp_close").on('click ontouchstart', function(e){
  		var body = $('body');
  		
  		body.toggleClass('nav_overlay');
    })


	/* ------------------------------------------------
	MASKEDINPUT START
	------------------------------------------------ */

		$(function($){
			$(".phone").mask("+7 (999) 999-99-99");
		});

	/* ------------------------------------------------
	MASKEDINPUT END
	------------------------------------------------ */

	/* ------------------------------------------------
	ACCORDION START
	------------------------------------------------ */

		if($('.accordion--item--link').length){
			$('.accordion--item--link').on('click', function(){
				$(this)
				.toggleClass('active')
				.next('.sub-menu')
				.slideToggle()
				.parents(".accordion--item")
				.siblings(".accordion--item")
				.find(".accordion--item--link")
				.removeClass("active")
				.next(".sub-menu")
				.slideUp();
			});  
		}
		$('.accordion--item--link').on('click', function(e) {
		    e.preventDefault();
		});


	/* ------------------------------------------------
	ACCORDION END
	------------------------------------------------ */


	/* ------------------------------------------------
	TABS START
	------------------------------------------------ */

		if ($("#horizontalTab").length){
			$("#horizontalTab").easyResponsiveTabs({
				type: 'horizontal'
			});
		}

	/* ------------------------------------------------
	TABS END
	------------------------------------------------ */


})